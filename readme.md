To whom it may concern,

This repo contains the part of the final group project that I was responsible for during this year's spring semester.

Please enjoy,

[Bancho Petrov](https://www.linkedin.com/in/bancho-petrov-910754115/)


## Brief Description

A small REST API that allows users to figure out which cities are mentioned in which English
books from Project Gutenberg and given a city which books mentioned it.

In essence, it supports the following queries:
- Given a city name your application returns all book titles with corresponding authors that mention this city.

- Given a book title, your application plots all cities mentioned in this book onto a map.

- Given an author name your application lists all books written by that author and plots all cities mentioned in any of the books onto a map.

- Given a geolocation, your application lists all books mentioning a city in vicinity of the given geolocation.


## Brief Code Description

*disclaimer: I have left out the parts of the project implemented by other group members and
will only be sharing and discussing code produced by me.*

The API is implemented using the combination of Node and Express.js. Two databases are
used in this project to handle the same data as this was one of the requirements � a
document-oriented one (MongoDB) and a graph one (Neo4j). The root of the project contains
the following folders:

- bin/

default location for Express.js startup scripts

- db/

contains queries executed against the two databases as well as a file establishing the
mongoDB connection

- performance_test/

script that runs all the queries against the two databases requesting the same data
and timing the responses in order to see which one performs better

- plotting/

python script used to plot cities on a map

- public/

default Express.js location for static files, not used for this project

- routes/

contains the Express.js routes that define the REST API endpoints

- scripts/

here is the logic that scans the book text files and finds cities mentioned in them as
well as corresponding author and book title; cities to be looked for are fetched from a mongo
collection

- test/

tests the mongo queries using Mocha (testing framework) and Chai (assertion
framework); when executed, a controlled test collection is used, instead of the production one
